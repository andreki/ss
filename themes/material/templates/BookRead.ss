<html>
<head>
    <% base_tag %>
    $MetaTags
    <% require javascript("https://cdn.jsdelivr.net/npm/epubjs/dist/epub.min.js") %>
    <% require javascript("https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.5/jszip.min.js") %>
    <% require themedCSS("css/bookRead.css") %>
</head>
<body>
    <% if $Document %>
    <div id="container">
        <div id="area"></div>
        <% include BookReadOverlay %>
        <script>
            $().ready(function () {
                // Render EPUB
                let book = ePub("$Document.AbsoluteURL");
                let rendition = book.renderTo("area", {
                    method: "default",
                    flow: "paginated",
                    width: "100%",
                    height: "100%"
                });

                // Key Bindings
                function keyListener(e) {
                    let kc = e.keyCode || e.which;
                    if (kc == 37) rendition.prev();
                    if (kc == 39) rendition.next();
                    if (kc == 13) toggleFullScreen();
                    if (kc == 27) goBack();
                }
                rendition.on("keyup", keyListener);
                $(document).on("keyup", keyListener);

                // Pagination
                $("#next").on("click", e => {
                    rendition.next();
                    e.preventDefault();
                });
                $("#prev").on("click", e => {
                    rendition.prev();
                    e.preventDefault();
                });

                // Render
                rendition.display();
            });
        </script>
    </div>
    <% end_if %>
</body>
</html>