<div class="card">
    <% if $CoverImage %>
        <a href="$Link">
            <% with $CoverImage.Fit(349, 200) %>
                    <img class="card-img-top img-thumbnail book-thumbnail" src="$URL" alt="$Title Cover"
                         width="$Width"
                         height="$Height">
            <% end_with %>
        </a>
    <% end_if %>
    <div class="card-body">
        <h5 class="card-title">
            <a href="$Link">
                $Title
            </a>
        </h5>
        <p class="card-text">
            <% if $Teaser %>
                $Teaser
            <% else %>
                $Content.FirstSentence
            <% end_if %>
        </p>
    </div>
    <ul class="list-group list-group-flush">
        <% if $Author %>
            <li class="list-group-item">
                <small class="text-muted">by</small> $Author
            </li>
        <% end_if %>
        <% if $PublishDate %>
            <li class="list-group-item">
                <small class="text-muted">published</small> $PublishDate.Nice
            </li>
        <% end_if %>
    </ul>
</div>