<div class="overlay">
    <script>
        function goBack() {
            window.history.back();
            <%--location.href = "$Parent.URL";--%>
        }

        function toggleFullScreen() {
            if (!document.webkitFullscreenElement) {
                document.documentElement.webkitRequestFullscreen();
            } else {
                if (document.webkitExitFullscreen) {
                    document.webkitExitFullscreen();
                }
            }
        }

        document.onwebkitfullscreenchange = function ( event ) {
            if (document.webkitIsFullScreen) {
                $('.btn-fullscreen i').text("fullscreen_exit");
                $(".overlay").css("visibility", "hidden");
            }
            else {
                $('.btn-fullscreen i').text("fullscreen");
                $(".overlay").css("visibility", "visible");
            }
        };

        // $(".overlay").on("click", e => {
        //     $(".overlay").css("visibility", $(".overlay").css("visibility")=="hidden" ? "visible" : "hidden");
        // });
    </script>
    <div class="fixed-top">

        <div class="btn-group" role="group" aria-label="Manage">
            <%--Close--%>
            <button type="button" class="btn btn-dark btn-lg" aria-label="Close" onclick="goBack()">
                <i class="material-icons">cancel</i>
            </button>

            <%--Fullscreen--%>
            <button type="button" class="btn btn-dark btn-lg btn-fullscreen" aria-pressed="true" onclick="toggleFullScreen()">
                <i class="material-icons">fullscreen</i>
            </button>
        </div>
    </div>

    <div class="fixed-bottom">
        <%--progress bar--%>
    </div>

    <div id="pagination">
        <a id="prev" href="#prev" class="arrow">
            <i class="material-icons">chevron_left</i>
        </a>
        <a id="next" href="#next" class="arrow">
            <i class="material-icons">chevron_right</i>
        </a>
    </div>
</div>