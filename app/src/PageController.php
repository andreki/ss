<?php

use SilverStripe\CMS\Controllers\ContentController;
use SilverStripe\Control\Director;
use SilverStripe\View\Requirements;

class PageController extends ContentController
{
    private static $allowed_actions = [];

    protected function init()
    {
        parent::init();

        Requirements::javascript('https://code.jquery.com/jquery-3.2.1.slim.min.js');
        Requirements::javascript('https://unpkg.com/popper.js@1.12.6/dist/umd/popper.js');
        Requirements::css('https://unpkg.com/bootstrap-material-design@4.1.1/dist/css/bootstrap-material-design.min.css');
        Requirements::javascript('https://unpkg.com/bootstrap-material-design@4.1.1/dist/js/bootstrap-material-design.js');

        Requirements::css('https://fonts.googleapis.com/icon?family=Material+Icons');

        Requirements::themedCSS('book');
    }
}
