<?php

class BookController extends PageController
{
    private static $allowed_actions = [
        'read'
    ];

    protected function init()
    {
        parent::init();
    }

    public function read(Silverstripe\Control\HTTPRequest $request)
    {
        return $this->renderWith("BookRead");
    }
}
